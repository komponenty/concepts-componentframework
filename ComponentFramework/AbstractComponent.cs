﻿using System;
using System.Collections.Generic;

namespace ComponentFramework
{
    public abstract class AbstractComponent : IComponent
    {
        #region private fields

        private IDictionary<Type, object> providedInterfaces;
        private ISet<Type> requiredInterfaces;

        #endregion

        #region constructors

        public AbstractComponent()
        {
            providedInterfaces = new Dictionary<Type, object>();
            requiredInterfaces = new HashSet<Type>();
        }

        #endregion

        #region IComponent implementation

        public IEnumerable<Type> ProvidedInterfaces
        {
            get { return new List<Type>(providedInterfaces.Keys).AsReadOnly(); }
        }

        public IEnumerable<Type> RequiredInterfaces
        {
            get { return new List<Type>(requiredInterfaces).AsReadOnly(); }
        }

        public object GetInterface(Type type)
        {
            if (providedInterfaces.ContainsKey(type))
                return providedInterfaces[type];
            return null;
        }

        public T GetInterface<T>() where T : class
        {
            Type type = typeof(T);
            return GetInterface(type) as T;
        }

        public abstract void InjectInterface(Type type, object impl);

        public void InjectInterface<T>(object impl) where T : class
        {
            Type type = typeof(T);
            InjectInterface(type, impl);
        }

        #endregion

        #region registration utils

        public void RegisterProvidedInterface(Type type, object impl)
        {
            if (!type.IsInterface)
                throw new ArgumentException(
                    string.Format("Registering type {0} is not an interface", type), 
                    "type");
            if (!type.IsInstanceOfType(impl))
                throw new ArgumentException(
                    string.Format("Registering provider does not provide object implementing {0} interface", type),
                    "provider");
            providedInterfaces[type] = impl;
        }

        public void RegisterProvidedInterface<T>(object impl) where T : class
        {
            RegisterProvidedInterface(typeof(T), impl);
        }

        public void RegisterRequiredInterface(Type type)
        {
            if (!type.IsInterface)
                throw new ArgumentException(
                    string.Format("Registering type {0} is not an interface", type),
                    "type");
            requiredInterfaces.Add(type);
        }

        public void RegisterRequiredInterface<T>() where T : class
        {
            RegisterRequiredInterface(typeof(T));
        }

        #endregion
    }
}
